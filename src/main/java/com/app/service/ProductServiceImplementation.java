package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ProductDao;
import com.app.pojos.Product;

@Service
@Transactional
public class ProductServiceImplementation implements ProductService{
	@Autowired
	private ProductDao productDao;
	
	@Override
	public List<Product> getAllProduct() {
		return productDao.getAllProduct();
	}

}

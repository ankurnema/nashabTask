package com.app.service;

import java.util.List;

import com.app.pojos.Customer;

public interface CustomerService {
	public List<Customer> getCustomers();
}

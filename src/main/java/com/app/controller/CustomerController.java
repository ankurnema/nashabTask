package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import com.app.pojos.*;
import com.app.service.CustomerService;

@RestController
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customer")
	public @ResponseBody List<Customer> getCustomer() {
		return customerService.getCustomers();
	}
	
	@GetMapping("/customer?output=csv")
	public @ResponseBody String getCustomerCSV(HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");
		List<Customer> customerList = customerService.getCustomers();
		StringBuilder sb = new StringBuilder();
		Iterator<Customer> iterator = customerList.iterator();
		while(iterator.hasNext()) {
			Customer customer = iterator.next();
			sb.append(customer.getCustomerId());
			sb.append(",");
			sb.append(customer.getCustomerName());
			sb.append(",");
			sb.append(customer.getCustomerEmail());
			sb.append(",");
			sb.append(customer.getCity());
			sb.append(",");
			sb.append(customer.getState());
			sb.append(",");
		}
		return sb.toString();
	}
}

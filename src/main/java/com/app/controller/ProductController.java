package com.app.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Iterator;

import com.app.pojos.Product;
import com.app.service.ProductService;

@RestController
//@RequestMapping("/products")
public class ProductController {
	@Autowired
	private ProductService productService;
	
	@GetMapping("/product")
	public @ResponseBody List<Product> getAllProduct(){
		return productService.getAllProduct();
	}
	
	@GetMapping(value="/product?output=csv")
	public @ResponseBody String getAllProductCSV(HttpServletResponse response) {
		response.setContentType("text/plain; charset=utf-8");
		List<Product> productList = productService.getAllProduct();
		StringBuilder sb = new StringBuilder();
		Iterator<Product> iterator = productList.iterator();
		while(iterator.hasNext()) {
			Product product = iterator.next();
			sb.append(product.getProductId());
			sb.append(",");
			sb.append(product.getProductCategory());
			sb.append(",");
			sb.append(product.getProductSubCategory());
			sb.append(",");
			sb.append(product.getDepartment());
			sb.append(",");
			sb.append(product.getFamily());
		}
		return sb.toString();
	}
}

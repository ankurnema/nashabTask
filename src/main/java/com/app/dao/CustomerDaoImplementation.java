package com.app.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.app.pojos.Customer;

@Repository
public class CustomerDaoImplementation implements CustomerDao{

	/*@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
		
	}*/
	
	//Above cpmmented  is throwing an excetion while creating bean for dataSource
			//So i used jdbcTemplate instead of datasource
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Customer> getCustomer() {
		return jdbcTemplate.query("select * from customer", new RowMapper<Customer>(){
			@Override
			public Customer mapRow(ResultSet rs, int rownumber) throws SQLException {
				Customer customer = new Customer();
				customer.setCustomerId(rs.getInt(1));
				customer.setCustomerName(rs.getString(2));
				customer.setCustomerEmail(rs.getString(3));
				customer.setCity(rs.getString(4));
				customer.setState(rs.getString(5));
				return customer;
			}
		});
	}
	
}

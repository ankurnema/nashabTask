package com.app.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.app.pojos.Customer;
import com.app.pojos.Product;

@Repository
public class ProductDaoImplementation implements ProductDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Product> getAllProduct() {
		return jdbcTemplate.query("select * from product", new RowMapper<Product>(){
			@Override
			public Product mapRow(ResultSet rs, int rownumber) throws SQLException {
				Product product = new Product();
				product.setProductId(rs.getInt(1));
				product.setProductCategory(rs.getString(2));
				product.setProductSubCategory(rs.getString(3));
				product.setDepartment(rs.getString(4));
				product.setFamily(rs.getString(5));
				return product;
			}
		});
	}

}

package com.app.dao;

import java.util.List;

import com.app.pojos.Customer;

public interface CustomerDao {
	public List<Customer> getCustomer();
}
